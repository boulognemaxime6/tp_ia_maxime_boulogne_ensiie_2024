# Infos importantes 

- L'affichage des noeuds explorés pendant l'execution et le détaille du chemin de la solution trouvé ont était implémenté dans toutes les fonction d'algorithme du programme. Cependant certain d'entre eux on étaient mit en commentaire pour rendre l'affichage du Main dans le terminale plus visble. il donc tous a fais possible de consulté ces fonctionnalité en enlevant les commentaires sur les lignes indiqué dans le programme.

- le projet fût compilé de la manière suivante :
  c:; cd 'c:\Users\boulo\OneDrive\Documents\S4 ENSIIE\IA\TP\tp_ia_maxime_boulogne_ensiie_2024'; & 'C:\Program Files\Java\jre1.8.0_301\bin\java.exe' '-cp' 'C:\Users\boulo\AppData\Roaming\Code\User\workspaceStorage\f8e5f0a92461a36b79af715c318742d5\redhat.java\jdt_ws\tp_ia_maxime_boulogne_ensiie_2024_597dfae6\bin' 'Main'

- TP aussi accessible sur un Git (comme pour les futurs de la matière) :
https://gitlab.com/boulognemaxime6/tp_ia_maxime_boulogne_ensiie_2024.git

# Résumé des réponses aux questions

# Exercice 1
1) Le jeu s'articule dans le code comme une classe plateau contenant un tableau 2 dimensions de cellule représentant les cases.
Avec des valeurs en entier et où 0 est la case vide (affiché "X" dans le terminal)

2) La fonction permettant cet affichage est showBoard(), permettant d'afficher la grille de manière visible et centrée suivant le nombre de chiffres maximum présent dans le jeu. Et ainsi cela permet un affichage adaptatif suivant la taille de la grille demandé.

3) Le type pour les opérateurs est défini à travers un ENUM ("Move.java"), ce qui nous permet d'utiliser des entrées bornées.

4) La fonction permettant le déplacement est la fonction move(Move move), elle prend en paramètre une direction définie suivant l'enum "Move"et ainsi échange la position de la case vide avec la case destination de la direction choisie et ce uniquement si cela est faisable (contenu dans les limites du plateau). Renvoie "True" si l'échange est effectué et "false" sinon

5) La fonction isFinalConfiguration() permet donc de vérifie le bon ordonnancement des valeur des case puis que la case vide se situe bien en bas à droite du plateau, dans le cas où tout serait respecté, renvoie "True" sinon renvoie "False".

6) L'implémentation de la fonction randomizeBoard() nous permet donc de générer une configuration aléatoire de jeu, cela est réalisé en déplaçant 10000 fois la case vide dans des directions aléatoires. l'initialisation du plateau étant faite sous forme d'une configuration final, ce mélange peut se faire depuis un état final, garantissant ainsi des solutions finisables. 

# Exercice 2
1) Nous utiliserons la classe SearchNode.java qui nous permet de représenter un nœud dans notre arbre de recherche. Pour la file, il s'agira d'une "LinkedList" 
d'objet SearchNode, afin de pouvoir réaliser nos recherches sans soucis.

2) La méthode breadthFirstSearch() parcourt les nœuds dans l'ordre de leur profondeur (en largeur d'abord), en ajoutant les successeurs de chaque nœud dans la file. Cette méthode explore les états. les plus proches de l'état initial en premier, garantissant la découverte d'une solution optimale.

3) Nous avons utilisé une pile (Stack) pour stocker les nœuds à explorer, ce que l'on vs utiliser pour toutes nos recherches en profondeurs.

4) Lors de la recherche en profondeur, la méthode depthFirstSearch() explore les nœuds en profondeur, en choisissant une branche et en descendant le plus loin possible avant de revenir en arrière. Cela peut conduire à une exploration en profondeur excessive, mais cela garantit la découverte d'une solution si elle existe.

5) La méthode depthLimitedSearch(int depthLimit) est une variante de la recherche en profondeur d'abord qui impose une limite de profondeur. Elle explore les nœuds jusqu'à une profondeur maximale spécifiée (depthLimit). Si aucune solution n'est trouvée dans cette limite de profondeur, elle arrête la recherche. Cela évite une exploration excessive en profondeur et ainsi gérer notre algorithme.

6) La méthode iterativeDeepeningSearch() quant à elle implémente la recherche en profondeur itérative en utilisant la méthode de recherche en profondeur limitée avec des limites de profondeur de plus en plus grandes à chaque itération. Cela permet de garantir que toutes les solutions sont trouvées et ainsi permettre de choisir la plus optimale suivant la taille du chemin et le nombre d'itérations.

7) À la fin de chacune des recherches, la taille du chemin menant à la solution ainsi que le nombre d'itérations qu'il a fallu pour trouver, la solution est affichée. De plus à travers la fonction : "printSolutionPath" il est possible de faire apparaître le détail du chemin trouvé (Par défaut, ce chemin est affiché uniquement sur la recherche en largeur pour des raisons de visibilité, mais est implémente en commentaire dans toutes les autres si besoin). Et pour finir, les recherches possèdent aussi l'affichage de la profondeur à laquelle l'exécution est, qui s'affiche lorsque que la profondeur est atteinte pour la première fois (et de la même manière cela est visible de base pour la recherche en largeur, mais peut être visible pour les autres en enlevant la partie en commentaire).

8) Dans le "Main.java" il y a une partie définie qui permet, au lancement de générer une configuration aléatoire (dont la taille est modifiable) et d'y appliquer tous nos algorithmes de recherche. Et ainsi pouvoir obtenir le résultat dans le terminal.

# Exercice 3 
1) Les foncions : "heuristic1", "heuristic2" et "heuristic3" implemente donc respectivement les 3 heuristiques vues en cour. Chacune d'entre elle retourne un score calculé suivant des conditions, qui sont de nouveau précisées dans les commentaires respectifs des fonctions.

2) Pour implémenter des files de priorité contenant des configurations, nous avons utilisé la classe PriorityQueue de Java. Cette classe nous permet de stocker des éléments de manière ordonnée en fonction de leur priorité.

3) L'algorithme A* est une méthode de recherche de chemin dans un graphe entre un nœud initial et un nœud final que nous avons implémenté à travers la fonction "aStar()". Il utilise une fonction heuristique (implementé précedemment) pour estimer le coût restant pour atteindre le nœud final à partir de chaque nœud. Dans notre implémentation, nous avons fait une méthode qui prend une heuristique en paramètre et utilise la file de priorité pour explorer les nœuds de l'arbre de recherche A* en fonction de leur priorité. Cette approche permet de guider la recherche vers les nœuds les plus prometteurs d'abord.

4) Nous avons instrumenté les fonctions pour récupérer le nombre de nœuds traités et le nombre de coups à jouer depuis l'état initial. À chaque fois qu'un nœud est exploré, nous incrémentons un compteur de nœuds traités.

5) Dans le main, on peut retrouver un test avec les 3 différentes heuristique à la suite, avec l'affichage de leurs résultats respectifs (il est toujours possible de voir leur chemin de solution en enlevant les "//" du commentaire dans la fonction "aStar()" comme pour toute les autres).