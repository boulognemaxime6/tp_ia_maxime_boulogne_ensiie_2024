import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.Stack;

public class Board { // Question 1.1
    private Cells[][] cells;
    private int boardSize;

    /**
     * Initialisation du plateau avec les chiffre dans l'ordre afin de pouvoir
     * melanger le tout ensuite (case vide en dernière case)
     * 
     * @param boardSize taille du plateau (nombre de cases = boardSize au carré)
     */
    public Board(int boardSize) {
        this.boardSize = boardSize;
        cells = new Cells[boardSize][boardSize]; // taille Carré
        // Initialisation du plateau avec les cellules dans l'ordre avant de mélanger
        int z = 1;
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (z == (boardSize * boardSize)) {
                    z = 0;
                }
                cells[i][j] = new Cells(z); // la valeure 0 représente une case vide
                z++;
            }
        }
    }

    /**
     * Constructeur prenant une configuration initiale spécifique .
     * 
     * @param initialConfiguration configuration du jeu, passée en paramètre sous
     *                             forme d'un tableau de cells
     */
    public Board(Cells[][] initialConfiguration) {
        this.boardSize = initialConfiguration.length;
        cells = new Cells[boardSize][boardSize];
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                cells[i][j] = initialConfiguration[i][j];
            }
        }
    }

    /**
     * Question 1.2
     * Fonction permattant d'afficher la grille de manière visible et centrée
     * suivant le nombre de chiffre maximum présent dans le jeu
     */
    public void showBoard() {
        // recupèrer le nombre de chiffre maximum présent dans la grille afin de gerer
        // le bon alignement et centrage des nombre
        int nbMax = boardSize * boardSize - 1; // valeur max
        String nbDigiMax = String.valueOf(nbMax); // nombre de chiffre de la valeur max
        // Parcours du tableau de cells
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                String currentCell = String.valueOf(cells[i][j].getvalue());
                if (currentCell.equals("0")) // afficher la case vide sous forme d'un 'X'
                    currentCell = "X";
                int nbSpaceToAdd = (nbDigiMax.length() - 1) - (currentCell.length() - 1);
                StringBuilder sb = new StringBuilder();
                // Ajoute les espaces au StringBuilder toujours dans l'optique de centrer les
                // chiffres
                for (int k = 0; k < nbSpaceToAdd; k++) {
                    sb.append(" ");
                }
                // Affichage du contenu de chaque cellule (+1 l'espacement des cases)
                System.out.print(" | " + currentCell + sb);
            }

            // Saut de ligne à la fin de chaque ligne du tableau
            System.out.print(" |\n");
        }
        System.out.println();
    }

    /**
     * Question 1.3
     * Applique un mouvement à la configuration actuelle du plateau.
     * 
     * @param move l'opérateur de déplacement à appliquer (définie dans l'enum
     *             "Move")
     * @return true si le mouvement est effectué avec succès, false sinon
     */
    public boolean move(Move move) {
        int emptyRow = -1;
        int emptyCol = -1;
        // Recherche de la position actuelle du trou (représenté par la valeur 0)
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (cells[i][j].getvalue() == 0) {
                    emptyRow = i;
                    emptyCol = j;
                }
            }
        }
        // Calcul des nouvelles positions après le déplacement suivant les different
        // input possibles
        int newRow = emptyRow;
        int newCol = emptyCol;
        switch (move) {
            case UP:
                newRow--;
                break;
            case RIGHT:
                newCol++;
                break;
            case DOWN:
                newRow++;
                break;
            case LEFT:
                newCol--;
                break;
        }

        // Vérifier si le déplacement est valide
        if (newRow < 0 || newRow >= boardSize || newCol < 0 || newCol >= boardSize) {
            // si le mouvement est en dehors des limites du plateau on retourne false
            // marquant l'erreur
            return false;
        }

        // Si déplacement valide, on échange les valeurs entre l'ancienne position du
        // trou et la nouvelle position
        Cells temp = cells[emptyRow][emptyCol];
        cells[emptyRow][emptyCol] = cells[newRow][newCol]; // le vide prend la place de la case destination
        cells[newRow][newCol] = temp; // la destination prend la place de la case vide

        return true;
    }

    /**
     * Question 1.4
     * Permet de verifie si la configuration du plateau est final (les chiffre dans
     * l'ordre avec la case vide en bas à droite)
     * 
     * @return True si final et False dans le cas contraire
     */
    public boolean isFinalConfiguration() {
        int expectedValue = 1; // afin de verifier la valeur des cases
        // Parcours du tableau de cells
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (i == cells.length - 1 && j == cells[i].length - 1) {// Dernière cellule, qui doit être la case vide
                    if (cells[i][j].getvalue() != 0) {
                        return false;
                    }
                } else {
                    // Les autres cellules doivent être dans l'ordre
                    if (cells[i][j].getvalue() != expectedValue) {
                        return false;
                    }
                    expectedValue++;
                }
            }
        }
        return true;
    }

    /**
     * Question 1.5
     * Permet de donner une configuration aléatoire du jeu en déplacant 10000 fois
     * la case vide de manière aléatoire
     */
    public void randomizeBoard() {
        Random random = new Random();
        // Effectuer 10 000 mouvements aléatoires
        for (int i = 0; i < 10000; i++) {
            // Générer un mouvement aléatoire
            int move = random.nextInt(4); // 0: haut, 1: droite, 2: bas, 3: gauche
            switch (move) {
                case 0:
                    move(Move.UP);
                    break;
                case 1:
                    move(Move.RIGHT);
                    break;
                case 2:
                    move(Move.DOWN);
                    break;
                case 3:
                    move(Move.LEFT);
                    break;
            }
        }
    }

    /**
     * Question 2.2
     * Imprime le chemin de la solution à partir du nœud final.
     */
    /**
     * 
     * @param finalNode  Dernier noeud exploré
     * @param iterations nombre d'itération réalisées une fois la solution trouvée
     * @param pathLength Profondeur du chemin exploré
     */
    private void printSolutionPath(SearchNode finalNode, int iterations, int pathLength) {
        // Liste pour stocker les nœuds du chemin de la solution
        List<SearchNode> path = new ArrayList<>();
        SearchNode currentNode = finalNode;

        // Remonte à travers les parents successifs jusqu'au nœud initial
        while (currentNode != null) {
            path.add(currentNode);
            currentNode = currentNode.parent;
        }

        // Affiche les configurations du chemin de la solution dans l'ordre inverse
        System.out.println("\nChemin de la solution :");
        for (int i = path.size() - 1; i >= 0; i--) {
            System.out.println("Étape " + (path.size() - i));
            Board board = new Board(path.get(i).configuration);
            board.showBoard();
        }

        // Affiche le nombre d'itérations et la longueur du chemin trouvé
        System.out.println("Solution trouvée !");
        System.out.println("Nombre d'itérations : " + iterations);
        System.out.println("Longueur du chemin trouvé : " + pathLength);
    }

    /***************** Recherche en largeur d’abord ******************/

    /**
     * Question 2.2
     * Recherche en largeur d'abord pour trouver la solution de la configuration
     */
    public void breadthFirstSearch() {

        Queue<SearchNode> queue = new LinkedList<>(); // implementation de la file Question 2.1

        Set<String> visited = new HashSet<>(); // permettre de voir les noeuds déjà visités et optimiser l'execution
        SearchNode initialNode = new SearchNode(cells, null, 0);

        queue.add(initialNode);
        visited.add(getConfigurationString(cells));

        int iterations = 0;
        int pathLength = 1;

        while (!queue.isEmpty()) {
            // Récupère le nœud en tête de file afin de le traiter
            SearchNode currentNode = queue.poll();

            // Vérifie si la configuration actuelle est finale et ainsi la solution
            if (new Board(currentNode.configuration).isFinalConfiguration()) {
                // Affiche le chemin de la solution, sa taille et le nombre de noeuds explorés
                printSolutionPath(currentNode, iterations, pathLength);
                return;
            }
            // afficher la profondeur quand on est en train de traiter un nœud à une
            // profondeur pas encore atteinte jusque là
            if (currentNode.depth == pathLength) {
                System.out.println("Profondeur atteinte pour la première fois : " + currentNode.depth);
                pathLength = currentNode.depth;
            }

            // Explore les mouvements possibles à partir de la configuration du noeud
            for (Move move : Move.values()) {
                Board newBoard = new Board(boardSize);
                newBoard.cells = cloneCells(currentNode.configuration);
                if (newBoard.move(move)) {
                    String configString = getConfigurationString(newBoard.cells);
                    if (!visited.contains(configString)) {
                        SearchNode newNode = new SearchNode(newBoard.cells, currentNode, currentNode.depth + 1);
                        queue.add(newNode);
                        visited.add(configString);
                        iterations++;
                        pathLength = newNode.depth;
                    }
                }
            }
        }
        System.out.println("Aucune solution trouvée");
    }

    /**
     * Copie de la configuration optimiser afin de ne pas dépasser la mémoire
     * 
     * @param original configuration a copier pour la construction de l'arbre
     * @return retourne le tableau de celulle cloné
     */
    private Cells[][] cloneCells(Cells[][] original) {
        Cells[][] clone = new Cells[boardSize][boardSize];
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                clone[i][j] = new Cells(original[i][j].getvalue());
            }
        }
        return clone;
    }

    /**
     * Méthode pour obtenir une représentation sous forme de chaîne de la
     * configuration du plateau.
     * 
     * @param cells La configuration du plateau
     * @return Une chaîne représentant la configuration du plateau donné
     */
    private String getConfigurationString(Cells[][] cells) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                sb.append(cells[i][j].getvalue());
            }
        }
        return sb.toString();
    }

    /***************** Recherche en profondeur d’abord ******************/

    // implementation de la pile contenant les configuration Question 2.3
    private Stack<SearchNode> stack = new Stack<>();

    /**
     * Question 2.4
     * Recherche en profondeur d'abord
     */
    public void depthFirstSearch() {
        stack.clear();
        Set<String> visited = new HashSet<>();
        SearchNode initialNode = new SearchNode(cells, null, 0);

        stack.push(initialNode);
        visited.add(getConfigurationString(cells));

        int iterations = 0;
        int pathLength = 0;

        while (!stack.isEmpty()) {
            SearchNode currentNode = stack.pop();

            if (new Board(currentNode.configuration).isFinalConfiguration()) {
                // !!! enlever le commentaire de cette ligne pour afficher le chemin de la
                // solution trouvé (très long)
                // printSolutionPath(currentNode, iterations, pathLength);

                // Affiche le nombre d'itérations et la longueur du chemin trouvé
                System.out.println("Solution trouvée !");
                System.out.println("Nombre d'itérations : " + iterations);
                System.out.println("Longueur du chemin trouvé : " + pathLength);
                return;
            }
            // afficher le num de la deph
            if (currentNode.depth == pathLength) {
                // !!! enlever le commentaire de cette ligne pour voir le numéro du noeud
                // exploré pour la première fois
                // System.out.println("Profondeur atteinte pour la première fois : " +
                // currentNode.depth);
                pathLength = currentNode.depth;
            }
            for (Move move : Move.values()) {
                Board newBoard = new Board(boardSize);
                newBoard.cells = cloneCells(currentNode.configuration);
                if (newBoard.move(move)) {
                    String configString = getConfigurationString(newBoard.cells);
                    if (!visited.contains(configString)) {
                        SearchNode newNode = new SearchNode(newBoard.cells, currentNode, currentNode.depth + 1);
                        stack.push(newNode);
                        visited.add(configString);
                        iterations++;
                        pathLength = newNode.depth;
                    }
                }
            }
        }

        System.out.println("Aucune solution trouvée");
    }

    /***************** Recherche en profondeur limité ******************/

    /**
     * Question 2.5
     * Recherche en profondeur limitée
     * 
     * @param depthLimit numéro de la profondeur limitée
     */
    public void depthLimitedSearch(int depthLimit) {
        stack.clear();
        Set<String> visited = new HashSet<>();
        SearchNode initialNode = new SearchNode(cells, null, 0);

        stack.push(initialNode);
        visited.add(getConfigurationString(cells));

        int iterations = 0;
        int pathLength = 0;

        while (!stack.isEmpty()) {
            SearchNode currentNode = stack.pop();

            if (currentNode.depth >= depthLimit) {
                continue;
            }

            if (new Board(currentNode.configuration).isFinalConfiguration()) {
                // printSolutionPath(currentNode, iterations, pathLength); !!!afficher le chemin
                System.out.println("Pour la profondeur : " + depthLimit + " Longueur du chemin trouvé : " + pathLength
                        + " Nombre d'itérations : " + iterations);
                return;
            }

            for (Move move : Move.values()) {
                Board newBoard = new Board(boardSize);
                newBoard.cells = cloneCells(currentNode.configuration);
                if (newBoard.move(move)) {
                    String configString = getConfigurationString(newBoard.cells);
                    if (!visited.contains(configString)) {
                        SearchNode newNode = new SearchNode(newBoard.cells, currentNode, currentNode.depth + 1);
                        stack.push(newNode);
                        visited.add(configString);
                        iterations++;
                        pathLength = newNode.depth;
                    }
                }
            }
        }

        System.out.println("Aucune solution pour la profondeur : " + depthLimit);
    }

    /***************** Recherche en profondeur itérative ******************/

    /*
     * Question 2.6
     * Recherche en profondeur itérative
     * Lance des recherche en profondeur limité avec des profondeurs différentes
     */
    public void iterativeDeepeningSearch(int n) {
        // test de la profondeur 0 à n
        for (int depthLimit = 0; depthLimit < n; depthLimit++) {
            depthLimitedSearch(depthLimit);
        }
    }

    /***************** implementation des herestique vues en cour *****************/

    /**
     * Question 3.1
     * Heuristic 1: Nombre de pièces mal placées,
     * Verifie le placement des differentes cases (meme la case vide) et incremente
     * un compteur pour chaque chiffre qui n'est pas à sa place finale
     * 
     * @return renvoie le compteur représentant le nombre de cases mal placées
     */
    public int heuristic1() {
        int misplacedPieces = 0;
        int expectedValue = 1;

        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                // on verifie le placement de la case vide (bas à droite si finale)
                if (i == boardSize - 1 && j == boardSize - 1) {
                    if (cells[i][j].getvalue() != 0) {
                        misplacedPieces++;
                    }
                } else { // verification du placement de chaque chiffres (cases)
                    if (cells[i][j].getvalue() != expectedValue) {
                        misplacedPieces++;
                    }
                    expectedValue++;
                }
            }
        }
        // renvoie le nombre de pièces mal placées (par rapport à une configuration
        // finale)
        return misplacedPieces;
    }

    /**
     * Question 3.1
     * Heuristic 2: Somme des distances de chaque pièce à sa position finale
     * 
     * @return la somme des distances calculée
     */
    public int heuristic2() {
        int totalDistance = 0;

        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                int value = cells[i][j].getvalue();
                // calcul de la distance à laquel est la case de son bon placement
                if (value != 0) {
                    int expectedRow = (value - 1) / boardSize;
                    int expectedCol = (value - 1) % boardSize;
                    totalDistance += Math.abs(i - expectedRow) + Math.abs(j - expectedCol);
                }
            }
        }

        return totalDistance;
    }

    //
    /**
     * Question 3.1
     * Heuristic 3: Somme des distances de chaque pièce à sa position finale, plus
     * le score s(n) définie suivant les regles :
     * -Case périphérique : 2 si pas suivie par son successeur 0 sinon
     * -Case centrale : 1 si non vide, 0 sinon
     * 
     * @return retourne la somme du score calculé et de l'heuristiques 2
     */
    public int heuristic3() {
        int h2 = heuristic2(); // heuristic 2
        int s = 0; // somme des scores suivant les types périphérique ou centrale

        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                int value = cells[i][j].getvalue();
                if (value != 0) {
                    int expectedRow = (value - 1) / boardSize;
                    int expectedCol = (value - 1) % boardSize;
                    int successorRow = (value) / boardSize;
                    int successorCol = (value) % boardSize;
                    int sToAdd = 0;

                    if (expectedRow == 0 || expectedRow == boardSize - 1 || expectedCol == 0
                            || expectedCol == boardSize - 1) {
                        // Si Case périphérique pas suivit par son successeur
                        if (successorRow != expectedRow || successorCol != expectedCol) {
                            sToAdd = 2; // on ajoute 2 point au score
                        } // sinon 0
                    } else {
                        // Si Case centrale non vide
                        if (cells[successorRow][successorCol].getvalue() != value + 1) {
                            sToAdd = 1; // on ajoute 1 point au score
                        } // sinon 0
                    }
                    s += sToAdd;
                }
            }
        }

        return h2 + 3 * s;
    }

    /***************** implementation de l'algorithme A* *****************/

    // Méthode pour exécuter l'algorithme A* avec une heuristique donnée
    /**
     * Question 3.3
     * @param heuristic
     */
    public void aStar(Heuristic heuristic) {
        // PriorityQueue pour stocker les nœuds à explorer, triés par profondeur
        PriorityQueue<SearchNode> priorityQueue = new PriorityQueue<>(
                (node1, node2) -> Integer.compare(node1.depth, node2.depth));
        // Ensemble pour stocker les configurations déjà visitées
        Set<String> visited = new HashSet<>();
        SearchNode initialNode = new SearchNode(cells, null, heuristic.calculate(this));
    
        // Ajout du nœud initial à la file de priorité et marquage de sa configuration comme visitée
        priorityQueue.add(initialNode);
        visited.add(getConfigurationString(cells));
    
        int iterations = 0;
        int pathLength = 0;
    
        while (!priorityQueue.isEmpty()) {
            // Extraction du nœud actuel de la file de priorité
            SearchNode currentNode = priorityQueue.poll();
    
            // Vérification si la configuration actuelle est la configuration finale
            if (new Board(currentNode.configuration).isFinalConfiguration()) {
                // Affichage de la solution trouvée
                System.out.println("Solution trouvée !");
                System.out.println("Nombre d'itérations : " + iterations);
                System.out.println("Longueur du chemin trouvé : " + pathLength);
                return;
            }
    
            // Affichage du numéro de la profondeur
            if (currentNode.depth == pathLength) {
                System.out.println("Profondeur atteinte pour la première fois : " +
                        currentNode.depth);
                pathLength = currentNode.depth;
            }
    
            // Exploration des mouvements possibles à partir de la configuration actuelle
            for (Move move : Move.values()) {
                Board newBoard = new Board(boardSize);
                newBoard.cells = cloneCells(currentNode.configuration);
                if (newBoard.move(move)) {
                    String configString = getConfigurationString(newBoard.cells);
                    // Vérification si la nouvelle configuration n'a pas été visitée auparavant
                    if (!visited.contains(configString)) {
                        // Création d'un nouveau nœud avec la nouvelle configuration
                        // et calcul de son coût en utilisant la fonction heuristique
                        SearchNode newNode = new SearchNode(newBoard.cells, currentNode,
                                heuristic.calculate(newBoard) + currentNode.depth + 1);
                        // Ajout du nouveau nœud à la file de priorité et marquage de sa configuration comme visitée
                        priorityQueue.add(newNode);
                        visited.add(configString);
                        // Mise à jour des compteurs
                        iterations++;
                        pathLength = newNode.depth;
                    }
                }
            }
        }
    
        System.out.println("No solution found.");
    }

}
