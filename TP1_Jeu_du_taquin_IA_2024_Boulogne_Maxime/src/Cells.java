//cellule représentant les cases du jeu possedant chacune une value en integer, où une value égal à  0 est la case vide
public class Cells {
    private int value;

    public Cells(int value) {
        this.value = value;
    }

    public int getvalue() {
        return this.value;
    }

    public void setvalue(int value) {
        this.value = value;
    }
}
