public class SearchNode {// Classe représentant un nœud de l'arbre de recherche. Question 2.1
    public Cells[][] configuration;
    public SearchNode parent;
    public int depth;

    public SearchNode(Cells[][] configuration, SearchNode parent, int depth) {
        this.configuration = configuration;
        this.parent = parent;
        this.depth = depth;
    }
}