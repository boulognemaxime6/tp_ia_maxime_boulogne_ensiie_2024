// Enumération pour représenter les différents opérateurs de déplacement
public enum Move {
    UP, // Déplacer vers le haut
    RIGHT, // droite
    DOWN, // bas
    LEFT // gauche
}