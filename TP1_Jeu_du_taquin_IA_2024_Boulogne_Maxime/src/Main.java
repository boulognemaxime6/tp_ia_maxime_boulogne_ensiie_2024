public class Main {
    public static void main(String[] args) {

        /******************** Test des algorithmes de recherche : ********************/

        // Création d'un plateau de jeu de taille carré (modifiable suivant l'input)
        Board B1 = new Board(3);
        // Génération de la configuration aléatoire utilisée
        B1.randomizeBoard();
        System.out.println("\n--Configuration aléatoire utilisée pour les recherches--\n");
        B1.showBoard();

        /***** Recherche en largeur d’abord *****/

        System.out.println("\n----------------Recherche en largeur d abord----------------\n");

        // Recherche en largeur d'abord pour trouver la solution
        System.out.println("Recherche en cours...");
        B1.breadthFirstSearch();

        /***** Recherche en profondeur d’abord *****/

        System.out.println("\n----------------Recherche en profondeur d abord----------------\n");

        // Recherche en profondeur d'abord pour trouver la solution
        System.out.println("Recherche en cours...\n");
        B1.depthFirstSearch();

        /***** Recherche en profondeur limitée *****/

        System.out.println("\n----------------Recherche en profondeur limitée----------------\n");

        // Recherche en profondeur limitée pour trouver la solution
        System.out.println("Recherche en cours...\n");
        B1.depthLimitedSearch(300);

        /***** Recherche en profondeur itérative *****/

        System.out.println("\n----------------Recherche en profondeur itérative----------------\n");

        // Recherche en profondeur limitée pour trouver la solution
        System.out.println("Recherche en cours...\n");
        B1.iterativeDeepeningSearch(100);

        /***** Algorithme A* *****/

        System.out.println("\n----------------Algorithme A* Heuristic 1----------------\n");

        Heuristic heuristic = new Heuristic1(); // Choisir l'heuristique à utiliser
        B1.aStar(heuristic);

        System.out.println("\n----------------Algorithme A* Heuristic 2----------------\n");

        Heuristic heuristic2 = new Heuristic2(); // Choisir l'heuristique à utiliser
        B1.aStar(heuristic2);

        System.out.println("\n----------------Algorithme A* Heuristic 3----------------\n");

        Heuristic heuristic3 = new Heuristic3(); // Choisir l'heuristique à utiliser
        B1.aStar(heuristic3);

    }
}
