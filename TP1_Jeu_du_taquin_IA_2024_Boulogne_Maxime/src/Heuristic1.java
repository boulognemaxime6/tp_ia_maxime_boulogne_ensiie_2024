public class Heuristic1 implements Heuristic {
    @Override
    public int calculate(Board board) {
        return board.heuristic1();
    }
}