public class Heuristic3 implements Heuristic {
    @Override
    public int calculate(Board board) {
        return board.heuristic3();
    }
}