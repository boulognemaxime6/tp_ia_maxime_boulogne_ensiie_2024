public interface Heuristic {
    int calculate(Board board);
}