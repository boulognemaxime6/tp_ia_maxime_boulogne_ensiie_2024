public class Heuristic2 implements Heuristic {
    @Override
    public int calculate(Board board) {
        return board.heuristic2();
    }
}